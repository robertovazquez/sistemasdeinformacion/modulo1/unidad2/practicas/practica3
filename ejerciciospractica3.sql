﻿USE roberpractica3;


-- 1 ejercicio


  SELECT e.dept_no,COUNT(*)empleados FROM emple e GROUP BY e.dept_no;

  -- 2 ejercicio

   SELECT DISTINCT e.dept_no,COUNT(*)empleados FROM emple e GROUP BY e.dept_no HAVING empleados>5;

  -- 3 ejercicio

SELECT DISTINCT e.dept_no, AVG(e.salario) mediasalario FROM emple e GROUP BY e.dept_no;

-- 4 ejercicio

  -- C1

  SELECT DISTINCT d.dept_no FROM depart d WHERE d.dnombre='VENTAS';

  SELECT DISTINCT e.apellido FROM emple e WHERE e.oficio='VENDEDOR' AND e.dept_no=(SELECT DISTINCT d.dept_no FROM depart d WHERE d.dnombre='VENTAS');


-- 5 ejercicio

   SELECT DISTINCT e.apellido,COUNT(*) vendedores FROM emple e WHERE e.oficio='VENDEDOR' AND e.dept_no=(SELECT DISTINCT d.dept_no FROM depart d WHERE d.dnombre='VENTAS');


-- 6 ejercicio
    

    SELECT DISTINCT d.dept_no FROM depart d WHERE d.dnombre='VENTAS';

    SELECT DISTINCT e.oficio FROM emple e WHERE e.dept_no=( SELECT DISTINCT d.dept_no FROM depart d WHERE d.dnombre='VENTAS');


    -- 7 ejercicio

   SELECT DISTINCT e.dept_no,COUNT(*)numempleados FROM emple e WHERE e.oficio='EMPLEADO' GROUP BY e.dept_no;


  -- 8 ejercicio

    -- c1
       SELECT DISTINCT e.dept_no, COUNT(*) empleados FROM emple e;
      
      -- c2
      
        SELECT DISTINCT MAX (empleados) maximo FROM (SELECT DISTINCT e.dept_no, COUNT(*) empleados FROM emple e)c1;

    -- final

      SELECT DISTINCT c1.dept_no FROM ( SELECT DISTINCT e.dept_no, COUNT(*) empleados FROM emple e) c1

       JOIN 
       (SELECT DISTINCT MAX (empleados) maximo FROM (SELECT DISTINCT e.dept_no, COUNT(*) empleados FROM emple e)c1)c2 ON c1.dept_no;


    -- 9 ejerccio

             -- c1

               SELECT DISTINCT AVG(e.salario)mediasalarios FROM emple e;

           -- c2

               SELECT DISTINCT e.dept_no,SUM(e.salario)sumasalario FROM emple e;

              -- final 


               SELECT DISTINCT * FROM ( SELECT DISTINCT AVG(e.salario)mediasalarios FROM emple e) c1 JOIN
                ( SELECT DISTINCT e.dept_no,SUM(e.salario)sumasalario FROM emple e) c2 ON
                c2.sumasalario>c1.mediasalarios;




               



        -- 10 ejercicio 

        SELECT DISTINCT e.oficio,SUM(e.salario) sumasalarios FROM emple e;

        -- ejercicio 11

          -- c1

          SELECT DISTINCT d.dept_no FROM depart d WHERE d.dnombre='VENTAS';

          -- final 
            SELECT DISTINCT e.oficio,SUM(e.salario) sumasalarios FROM emple e WHERE e.dept_no=(SELECT DISTINCT d.dept_no FROM depart d WHERE d.dnombre='VENTAS');


    -- ejercicio 12

            -- c1

            SELECT e.dept_no, COUNT(*) empleados FROM emple e WHERE e.oficio='EMPLEADOS' ORDER BY empleados ;

          -- c2

             SELECT c1.dept_no, MAX(c1.empleados) maximo FROM ( SELECT e.dept_no, COUNT(*) empleados FROM emple e WHERE e.oficio='EMPLEADOS' ORDER BY empleados )c1;

            -- final

              SELECT c1.dept_no  FROM ( SELECT e.dept_no, COUNT(*) empleados FROM emple e WHERE e.oficio='EMPLEADOS' ORDER BY empleados )c1 JOIN
                (SELECT c1.dept_no, MAX(c1.empleados) maximo FROM ( SELECT e.dept_no, COUNT(*) empleados FROM emple e WHERE e.oficio='EMPLEADOS'ORDER BY empleados )c1)c2 ON
                 c1.empleados=c2.maximo;
                 



      -- ejercicio 13

      SELECT e.dept_no, COUNT(DISTINCT e.oficio)oficios FROM emple e; 

      -- ejercicio 14

     SELECT e.dept_no,e.oficio,COUNT(*)numeroempleado FROM emple e WHERE e.dept_no>2;


    -- ejercicio 15

      SELECT DISTINCT h.estanteria,SUM(h.unidades) unidades FROM herramientas h;


-- ejercicio 16
  
     -- c1      
       
         SELECT h.estanteria,SUM(h.unidades) unidades FROM herramientas h;

         -- c2

          SELECT MAX(c1.unidades) maximo FROM ( SELECT h.estanteria,SUM(h.unidades) unidades FROM herramientas h)c1;


          -- total

           SELECT c1.estanteria
            FROM ( SELECT h.estanteria,SUM(h.unidades) unidades FROM herramientas h)c1 JOIN 
            ( SELECT MAX(c1.unidades) maximo FROM ( SELECT h.estanteria,SUM(h.unidades) unidades FROM herramientas h)c1)c2
            ON c1.unidades=c2.maximo;

         
        
        
          

  -- ejercicio 17

  SELECT DISTINCT m.cod_hospital des, COUNT(*) numeromedicos FROM medicos m; 

 -- ejercicio 18


   SELECT m.especialidad FROM medicos m;

  -- ejercicio 19

    SELECT DISTINCT m.especialidad,COUNT(*)nummedicos FROM medicos m;

-- ejercicio 20

  SELECT DISTINCT p.cod_hospital,COUNT(*) empleados FROM personas p;


  -- ejercicio 21

    SELECT DISTINCT m.especialidad,COUNT(*)empleados FROM medicos m;

    -- ejercicio 22

 -- c1

    SELECT m.especialidad,COUNT(*) empleados FROM medicos m GROUP BY m.especialidad;

    -- c2

    SELECT MAX(empleados) maximo FROM (SELECT m.especialidad,COUNT(*) empleados FROM medicos m GROUP BY m.especialidad)c1;


-- final 

    SELECT  c1.especialidad FROM ( SELECT m.especialidad,COUNT(*) empleados FROM medicos m GROUP BY m.especialidad)c1 JOIN
      (SELECT MAX(empleados) maximo FROM (SELECT m.especialidad,COUNT(*) empleados FROM medicos m GROUP BY m.especialidad)c1) c2
      ON c1.empleados=c2.maximo;

    -- ejercicio 23

       -- c1
         
         SELECT DISTINCT MAX(h.num_plazas) maximo FROM hospitales h;

       -- final

        SELECT DISTINCT h.nombre FROM hospitales h WHERE h.num_plazas=(SELECT DISTINCT MAX(h.num_plazas) maximo FROM hospitales h);

       -- ejercicio 24

        SELECT DISTINCT h.estanteria FROM herramientas h ORDER BY h.estanteria DESC;

      -- ejercicio 25

         SELECT h.estanteria,SUM(h.unidades) unidades FROM herramientas h;

        --  ejercicio 26

       SELECT h.estanteria, SUM(h.unidades) mayor FROM herramientas h WHERE h.unidades>15;

  -- ejercicio 27

      -- c1
         SELECT h.estanteria, COUNT(h.unidades) unidades FROM herramientas h GROUP BY h.estanteria;

      -- c2

         SELECT MAX(unidades) maximo  FROM ( SELECT h.estanteria, COUNT(h.unidades) unidades FROM herramientas h GROUP BY h.estanteria)c1;
     
           
       -- final

        SELECT c1.estanteria FROM ( SELECT h.estanteria, COUNT(h.unidades) unidades FROM herramientas h GROUP BY h.estanteria)c1 JOIN
          (SELECT MAX(unidades) maximo  FROM ( SELECT h.estanteria, COUNT(h.unidades) unidades FROM herramientas h GROUP BY h.estanteria)c1)c2 
          ON c1.unidades=c2.maximo;


 -- 28 ejercicio

         SELECT d.dept_no,d.dnombre,d.loc FROM depart d LEFT JOIN emple e ON d.dept_no = e.dept_no;

  -- 29 ejercicio

       SELECT d.dept_no, COUNT(d.dept_no) empleados FROM depart d LEFT JOIN emple e ON d.dept_no = e.emp_no;


-- 30 ejercicio 

        SELECT d.dept_no,d.dnombre,SUM(e.salario) sumasalario FROM depart d LEFT JOIN emple e ON d.dept_no;

-- 31 ejercicio


     SELECT d.dept_no,d.dnombre,SUM(IFNULL(e.salario,0)) sumasalario FROM depart d LEFT JOIN emple e ON d.dept_no;


 -- 32 ejercicio
  
     SELECT DISTINCT h.cod_hospital,h.nombre,COUNT(h.cod_hospital) nummedicos  FROM hospitales h LEFT JOIN medicos m ON h.cod_hospital = m.cod_hospital;


      
   
      
    
     









 
  






       


















